# Photo Album CSS Box Model Practice #

Using CSS to manipulate the spacing and styling of elements in a HTML document.

### Clone Repository ###

1. Fork this repository.
2. Git clone your own copy to your assignments folder.
3. Change directory into your local copy of the repo.

### To Do ###

1. Open your local copy folder with Sublime, there is a photos folder, a HTML document and a CSS style sheet.
2. Add your style rules to style.css, this file is linked to from photo-album.html.
3. Open photo-album.html by typing in terminal 'open photo-album.html', notice the way the example photos are styled.
4. Remove the example photo elements from photo-album.html and their styles from style.css.
5. Add your own photos to the photos folder, add them to the HTML document, style them using all the techniques you have learned.
6. You can see your progress by typing in terminal 'open photo-album.html'.
